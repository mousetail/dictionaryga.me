import time

import requests

host = 'https://dictionaryga.me'

if __name__ == "__main__":
    exluded_words = set()
    with open('res/words-100k.txt', encoding='utf-8') as f:
        for line in f:
            if line and not line.startswith('#'):
                exluded_words.add(line.strip())

    ses = requests.session()
    ses.get(host).raise_for_status()
    csrf_cookie = ses.cookies.get('csrftoken')
    print(csrf_cookie)
    res = ses.post(f'{host}/api/start_game', headers={'X-CSRFtoken': csrf_cookie, 'Referer': host}, json={})
    print(res.status_code)
    res.raise_for_status()

    with open('res/words.txt', encoding='utf-8') as f:
        for index, line in enumerate(f):
            word = line.strip()
            if index % 9 != 0 and word not in exluded_words and word > 'deadhand':#'aciculum':

                res = ses.post(
                    f'{host}/api/new_word', headers={'X-CSRFTOKEN': csrf_cookie, 'Referer': host},
                    json={
                        'word': word,
                        'definition': ''
                    }
                )

                if res.status_code != 200:
                    print(f"\t {word} invalid response: ", res.status_code)
                    if res.status_code == 500:
                        time.sleep(1.5)
                else:
                    print(word)
                    time.sleep(0.25)
