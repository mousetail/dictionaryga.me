import sys, subprocess
import select
import os
import json
import hashlib

import prettyprinter


class Hasher():
    def __init__(self):
        if not os.path.isdir('tmp'):
            os.mkdir('tmp')
        if os.path.exists('tmp/hashes.json'):
            with open('tmp/hashes.json') as f:
                self.hashes = json.load(f)
        else:
            self.hashes = {}

    def get_file_changed(self, file):
        with open(file, 'rb') as f:
            hash = hashlib.md5()
            hash.update(f.read())
            hash = hash.hexdigest()
        if file in self.hashes:
            if self.hashes[file] == hash:
                return False
        self.hashes[file] = hash
        return True

    def get_directory_changed(self, directory, extensions=None, pretty=None):
        changed = False
        for root, dirs, files in os.walk(directory):
            for file in files:
                if not extensions or any(file.endswith(i) for i in extensions):
                    if self.get_file_changed(os.path.join(root, file)):
                        changed = True
                        if pretty:
                            pretty.message('changed ' + os.path.join(root, file))
        return changed

    def save(self):
        with open('tmp/hashes.json', 'w') as f:
            json.dump(self.hashes, f)


def run(arguments, pretty):
    pretty.message(">" + " ".join(arguments))
    process = subprocess.Popen(arguments, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    allout = ""
    lastout = ""
    allerr = ""
    lasterr = ""

    while process.returncode is None:
        process.poll()

        pipes, _, _ = select.select([process.stdout, process.stderr], [], [], 0.1)
        if process.stdout in pipes:
            res = process.stdout.read()
            lastout += res
            allout += res
        if process.stderr in pipes:
            res = process.stderr.read()
            lasterr += res
            allerr += res

        if '\n' in lasterr:
            lasterr = lasterr.split('\n')
            lines = lasterr[:-1]
            for line in lines:
                pretty.message("\t" + line)
            lasterr = lasterr[-1]
        if '\n' in lastout:
            lastout = lastout.split('\n')
            lines = lastout[:-1]
            for line in lines:
                pretty.message("\t" + line)
            lastout = lastout[-1]
    if lasterr:
        pretty.error('\t' + lasterr)
    if lastout:
        pretty.message('\t' + lastout)

    # process.wait()
    if process.returncode != 0:
        raise SystemError("Non-zero return code: " + str(process.returncode))

    return allout, allerr


def deploy():
    with prettyprinter.Pretty() as pretty:
        pretty.status("Loading hashes...")
        hasher = Hasher()
        try:
            pretty.done()

            pretty.status("Getting new files from git...")
            run(["git", "checkout", "."], pretty)
            run(["git", "pull"], pretty)
            pretty.done()
            if hasher.get_file_changed('package-lock.json'):
                pretty.status("Updating JS...")
                run(["npm", "ci"], pretty)
                pretty.done()
            else:
                pretty.status("No new JS dependencies")
                pretty.done()
            pretty.status("Building Bundle...")
            if hasher.get_directory_changed('frontend'):
                run(["npm", "run", "release"], pretty)
            else:
                pretty.warning("No changes detected")
            pretty.done()
            if hasher.get_file_changed('requirements.txt'):
                pretty.status("Updating python...")
                run(["pip", "install", "-r", "requirements.txt"], pretty)
                pretty.done()
            pretty.done()
            pretty.status("Running migrations...")
            run(["python", "manage.py", "migrate"], pretty)
            pretty.done()
            pretty.status("Collecting static files...")
            run(["python", "manage.py", "collectstatic", "--noinput"], pretty)
            pretty.done()

            pretty.message("DONE!")
            pretty.message("Don't forget to hit 'reload' in the python-anywhere control panel")
        finally:
            hasher.save()


if __name__ == "__main__":
    if sys.version_info < (3, 0):
        print("Are you sure you activated the virtual env?")
    elif input("Are you sure you want to deploy? (Y/N)").upper() in ("Y", "YES"):
        deploy()
