interface User {
    username: string | undefined;
    hasAccount: boolean;
    points: number;
    uid: string;
}