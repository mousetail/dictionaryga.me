interface Word {
    word: string,
    pos: 'N' | 'ADJ' | 'ADV' | 'V' | undefined;
}