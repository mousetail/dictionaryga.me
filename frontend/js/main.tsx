import * as React from "react";

import {get_cookie} from "./util";
import {GuessDefinition} from './toplevel/guessDefinition';
import {WriteDefinition} from "./toplevel/writeDefinition";
import {NewWord} from "./toplevel/newWord";
import {StartPage} from "./toplevel/startPage";
import {CSSTransition, TransitionGroup} from 'react-transition-group';
import {Score} from "./widgets/score";
import {Menu} from "./widgets/menu";
import {LoginScreen} from "./toplevel/LoginScreen";
import {UserPage} from "./toplevel/UserPage";
import {Leaderboard} from "./toplevel/leaderboard";
import {request} from "./request";
import {AboutPage} from "./toplevel/aboutPage";
import {ErrorWidget} from "./widgets/error";

interface MainProps {

}

interface MainState {
    loading: boolean,
    word: undefined | Word,
    definitions: Array<[number, string, number]>;
    mode: string
    freeze: boolean,
    page: string,
    user: User,
    error: string | undefined;
    menu_active: boolean;
}

export class Main extends React.PureComponent<MainProps, MainState> {
    constructor(props: {}) {
        super(props);

        this.state = {
            'loading': false,
            'word': undefined,
            'definitions': undefined,
            'mode': undefined,
            'freeze': false,
            'user': {
                'username': undefined,
                'hasAccount': false,
                'uid': localStorage.getItem('uid'),
                'points': undefined
            },
            'page': 'menu',
            'error': undefined,
            'menu_active': false
        };

        this.next = this.next.bind(this);
        this.setPoints = this.setPoints.bind(this);
        this.changePage = this.changePage.bind(this);
        this.activateMenu = this.activateMenu.bind(this);
        this.setUser = this.setUser.bind(this);
    }

    componentDidMount(): void {
    }

    setUser(user: Partial<User>) {
        this.setState(state => ({
            user: {...state.user, ...user}
        }))
    }

    setPoints(points: number) {
        this.setState(
            state => ({
                user: {
                    ...state.user,
                    points: points
                }
            }));
    }

    changePage(page: string) {
        if (page == '' && this.state.word == undefined) {
            this.startGame();
        } else {

            this.setState({
                page: page,
                menu_active: false
            });
        }
    }

    activateMenu() {
        this.setState({
            menu_active: !this.state.menu_active
        })
    }

    next() {
        this.setState({
            loading: true
        });

        request(
            '/api/word',
        ).then((data) => {
            if (data.mode === undefined) {
                console.log('bad mode');
                throw new Error("Bad data");
            }
            this.setState({
                'loading': false,
                'word': data.word,
                'mode': data.mode,
                'definitions': data.definitions
            })
        }).catch((error) => {
            this.setState({
                error: error
            })
        })
    }

    startGame() {
        this.setState({
            'loading': true,
            'page': ''
        });

        request('/api/start_game',
            {
                uid: localStorage.getItem('uid')
            }
        ).then(
            (result) => {
                let uid = result.uid;
                localStorage.setItem('uid', uid);
                this.setState(
                    {
                        user: result.user,
                        page: '',
                    }
                );
                this.next();
            }
        ).catch(
            err => this.setState({error: "" + err})
        )
    }

    renderPage(): JSX.Element {
        if (this.state.page === "menu") {
            return <StartPage
                onStart={this.startGame.bind(this)}
                onPageChange={this.changePage}
            />
        } else if (this.state.page == 'login') {
            return <LoginScreen
                onPageChange={this.changePage}
                onUserChange={this.setUser}
            />
        } else if (this.state.page === 'new') {
            return <NewWord
                key='new'
                onPageChange={this.changePage}
            />
        } else if (this.state.page == 'account') {
            return <UserPage
                user={this.state.user}
                setUser={this.setUser}
                onPageChange={this.changePage}
            />
        } else if (this.state.page == 'leaderboard') {

            return <Leaderboard
                onPageChange={this.changePage}
                user={this.state.user}
            />
        } else if (this.state.page == 'about') {
            return <AboutPage onPageChange={this.changePage}/>
        } else if (this.state.page == '') {
            if (this.state.mode === 'guess') {
                return <GuessDefinition
                    key={'guess-' + this.state.word}
                    mode={this.state.mode}
                    word={this.state.word}
                    definitions={this.state.definitions}
                    setPoints={this.setPoints}
                    next={this.next}
                />
            } else if (this.state.mode === 'write') {
                return <WriteDefinition
                    key={'write-' + this.state.word}
                    mode={this.state.mode}
                    word={this.state.word}
                    setPoints={this.setPoints}
                    next={this.next}
                />
            } else {
                console.log('invalid state: ' + this.state.mode);
                return <h1 key='error'>Invalid state: {this.state.mode}</h1>
            }
        } else {
            return <h1>Invalid page: {this.state.page}</h1>
        }

    }

    render() {
        return <div className='container'>
            <ErrorWidget error={this.state.error}/>
            {this.state.user.points !== undefined &&
            <Score score={this.state.user.points}
                   activateMenu={this.activateMenu}
            />}
            <TransitionGroup>
                {!this.state.loading &&
                <CSSTransition
                  component={null}
                  classNames="left"
                  appear={false}
                  enter={true}
                  exit={true}
                  timeout={{enter: 500, exit: 500}}
                  key={this.state.loading ? 'loading' : this.state.page + '-' + this.state.mode + '-' + this.state.word?.word}
                >
                  <div className={"transition-inner-container"}>
                      {this.renderPage()}
                  </div>
                </CSSTransition>}
            </TransitionGroup>
            {
                this.state.menu_active &&
                <Menu
                  onPageChange={this.changePage}
                />
            }
        </div>
    }
}