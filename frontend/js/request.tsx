import {get_cookie} from "./util";

export async function request(url: string, data?: object | undefined) {
    let response = await fetch(url, {
        'method': data === undefined ? 'GET' : 'POST',
        'body': JSON.stringify(data),
        'credentials': "same-origin",
        'headers': {
            'X-CSRFToken': get_cookie('csrftoken'),
            'Content-Type': 'application/json'
        }
    });
    if (response.ok) {
        return await response.json()
    } else if (response.status <= 499) {
        throw new Error(await response.text());
    } else {
        throw new Error(response.statusText)
    }
}