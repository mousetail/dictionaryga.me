import {faFacebook} from "@fortawesome/free-brands-svg-icons/faFacebook";
import {faReddit} from "@fortawesome/free-brands-svg-icons/faReddit";
import {IconDefinition} from "@fortawesome/fontawesome-svg-core";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFacebookSquare} from "@fortawesome/free-brands-svg-icons/faFacebookSquare";
import * as React from "react";

interface SocialButton {
    url: string;
    name: string;
    icon: IconDefinition
}

const socialLinks: SocialButton[] = [
    {
        url: 'https://www.facebook.com/dictionarygame/',
        name: 'facebook',
        icon: faFacebook
    },
    {
        url: 'https://www.reddit.com/r/dictionarygame/',
        name: 'reddit',
        icon: faReddit
    }
];

export function SocialButtons({fixed}: { fixed: boolean }) {
    return <div className={"social-buttons" + (fixed ? ' social-buttons-fixed' : '')}>
        <a href="https://www.facebook.com/dictionarygame/"
           target="_blank"
           rel="nofollow noopener noreferer"
        >
            <FontAwesomeIcon icon={faFacebookSquare}/>
        </a>
        <a href="https://www.reddit.com/r/dictionarygame/"
           target="_blank"
           rel="nofollow noopener noreferer">
            <FontAwesomeIcon icon={faReddit}/>
        </a>
    </div>
}