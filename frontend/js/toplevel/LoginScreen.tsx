import * as React from 'react';
import {get_cookie} from "../util";
import {request} from "../request";

interface LoginScreenProps {
    onPageChange: (page: string) => void;
    onUserChange: (user: User) => void;
}

interface LoginScreenState {
    username: string,
    password: string,
    error: string | undefined
}

export class LoginScreen extends React.Component<LoginScreenProps, LoginScreenState> {
    constructor(props: LoginScreenProps) {
        super(props);
        this.state = {
            username: '',
            password: '',
            error: undefined
        };

        this.onLogIn = this.onLogIn.bind(this);
    }

    onLogIn() {
        request('/api/login',
            {
                username: this.state.username,
                password: this.state.password
            }
        ).then(
            (data) => {
                this.props.onUserChange(
                    data.user
                );
                this.props.onPageChange(
                    ''
                )
            }
        ).catch(
            (error) => {
                this.setState({
                    error: "" + error
                })
            }
        )
    }

    render(): JSX.Element {
        return <div>

            <form className="form">
                {this.state.error}
                <label>
                    Username:
                    <input
                        placeholder="Username"
                        value={this.state.username}
                        autoComplete="username"
                        onChange={(ev) => {
                            this.setState({
                                username: ev.target.value
                            })
                        }}
                    />
                </label>
                <label>
                    Password:
                    <input
                        type='password'
                        placeholder="password"
                        value={this.state.password}
                        onChange={
                            (ev) => {
                                this.setState({
                                    password: ev.target.value
                                })
                            }
                        }/>
                </label>
            </form>
            <button className="submit-button"
                    onClick={this.onLogIn}
            >
                Log in
            </button>
            <a href="#" className="submit-button"
               onClick={
                   (ev) => {
                       ev.preventDefault();
                       this.props.onPageChange('')
                   }
               }>
                Start fresh
            </a>
        </div>
    }
}