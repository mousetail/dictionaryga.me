import * as React from 'react';
import {get_cookie} from "../util";
import {request} from "../request";
import {SocialButtons} from "../socialButtons";


const GreekLetters = [
    "Alpha",
    "Beta",
    "Gamma",
    "Delta",
    "Epsilon",
    "Zeta",
    "Eta",
    "Theta",
    "Iota",
    "Kappa",
    "Lambda",
    "Mu",
    "Nu",
    "Xi",
    "Pi",
    "Rho",
    "Phi",
    "Sigma",
    "Tau",
    "Upsilon",
    "Phi",
    "Chi",
    "Psi",
    "Omega"
];

const PhoneticAlphabet = [
    "Altitude",
    "Bravo",
    "Charlie",
    "David",
    "Everest",
    "Foxtrot",
    "Freedom",
    "Grip",
    "Golf",
    "Hotel",
    "Italy",
    "Joyride",
    "Juliet",
    "King",
    "Kilo",
    "Lama",
    "Mike",
    "Mathilde",
    "Nosebleed",
    "November",
    "Otter",
    "Pastiche",
    "Quebec",
    "Romeo",
    "Rodeo",
    "Tango",
    "Sidewalk",
    "Sierra",
    "Trampoline",
    "Utopia",
    "Uniform",
    "Venus",
    "Whiskey",
    "Xray",
    "Yankee",
    "Zebra"
];

function choice(sequence: string[]) {
    return sequence[Math.floor(Math.random() * sequence.length)];
}

interface UserPageProps {
    user: User;
    setUser: (u: Partial<User>) => void,
    onPageChange: (page: string) => void,
}

interface UserPageState {
    error: string | undefined,
    usernameHint: string,
    username: string;
    password: string;
    password2: string;
    freeze: boolean;
}

export class UserPage extends React.PureComponent<UserPageProps, UserPageState> {
    constructor(props: UserPageProps) {
        super(props);

        this.state = {
            error: undefined,
            usernameHint: choice(GreekLetters) + choice(PhoneticAlphabet) + Math.floor(Math.random() * 1000),
            username: '',
            password: '',
            password2: '',
            freeze: false
        };

        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit() {
        if (this.state.freeze) {
            return;
        }
        let error;
        if (this.state.username === '') {
            error = "Please enter a username. Out of ideas? Try " + this.state.usernameHint
        } else if (this.state.password === '') {
            error = "Please enter a password"
        } else if (this.state.password2 !== this.state.password) {
            error = "Passwords do not match"
        }

        if (error) {
            this.setState({
                error: error
            });
            return;
        }
        this.setState({
            freeze: true
        });

        request(
            '/api/register',
            {
                username: this.state.username,
                password: this.state.password
            }
        ).then(
            data => {
                this.props.setUser({
                    username: data.user.username,
                    points: data.user.points,
                    hasAccount: data.user.hasAccount
                })
            }
        ).catch(
            error => {
                this.setState({
                    error: "" + error
                })
            }
        )
    }

    render(): JSX.Element {
        if (!this.props.user.hasAccount) {
            return <div>
                <h1>You do not have an account yet</h1>
                <p className={'fading-p'}>
                    Create an account to save your progress, get listed on leaderboards,
                    and to play on multiple devices.
                </p>

                <div className={'form'}>
                    {this.state.error}

                    <label>
                        Username:
                        <input
                            placeholder={"Try " + this.state.usernameHint}
                            value={this.state.username}
                            onChange={(ev) => {
                                this.setState({
                                    username: ev.target.value
                                })
                            }}
                        />
                    </label>
                    <label>
                        Password:
                        <input type="password"
                               autoComplete="new-password"
                               placeholder="Enter a Password"
                               value={this.state.password}
                               onChange={(ev) => {
                                   this.setState({
                                       password: ev.target.value
                                   })
                               }}
                        />
                    </label>
                    <label>
                        Confirm password:
                        <input type="password"
                               autoComplete="new-password"
                               placeholder="Repeat password"
                               value={this.state.password2}
                               onChange={(ev) => {
                                   this.setState({
                                       password2: ev.target.value
                                   })
                               }}
                        />
                    </label>
                </div>

                <div className="button-row">
                    <a href="#" onClick={(ev) => {
                        ev.preventDefault();
                        this.props.onPageChange('')
                    }
                    }>
                        Back to game
                    </a>
                    <button
                        onClick={this.onSubmit}
                    >Register
                    </button>
                </div>
            </div>
        }

        return <div>
            <h1>Welcome, {this.props.user.username}</h1>
            <div className="player-score">
                <div>
                    <div className={"single-point"}>
                        {this.props.user.points}
                    </div>
                    <div className={"point-label"}>
                        Points this week
                    </div>
                </div>

                <div>
                    <div className={"single-point"}>
                        {this.props.user.points}
                    </div>
                    <div className={"point-label"}>
                        Points this month
                    </div>
                </div>

                <div>
                    <div className={"single-point"}>
                        {this.props.user.points}
                    </div>
                    <div className={"point-label"}>
                        Points this year
                    </div>
                </div>

                <div>
                    <div className={"single-point"}>
                        {this.props.user.points}
                    </div>
                    <div className={"point-label"}>
                        Total points
                    </div>
                </div>


            </div>

            <SocialButtons fixed={false}/>

            <div className={"button-row"}>
                <a href="#">
                    log out
                </a>

                <button
                    onClick={
                        () => {
                            this.props.onPageChange('leaderboard')
                        }
                    }
                >View Leaderboard
                </button>

                <button
                    onClick={() => {
                        this.props.onPageChange('')
                    }
                    }
                >Continue Playing
                </button>

            </div>
        </div>
    }
}