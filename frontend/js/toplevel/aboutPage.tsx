import * as React from 'react';
import {SocialButtons} from "../socialButtons";

interface AboutPageProps {
    onPageChange: (p: string) => void;
}

interface AboutPageState {

}

export class AboutPage extends React.PureComponent<AboutPageProps, AboutPageState> {
    render() {
        return <div>
            <h1>How it works</h1>
            <p className={'fading-p'}>
                Dictionary game is a game about cool and unusual english words. Uses can write their guesses for
                plausible definitions and other can guess which definitions are made up and which are real.
            </p>

            <SocialButtons fixed={false}/>

            <div className="button-row">
                <button
                    onClick={
                        () => this.props.onPageChange('')
                    }
                >Back to game
                </button>
            </div>
        </div>
    }
}