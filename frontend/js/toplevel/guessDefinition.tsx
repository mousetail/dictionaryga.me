import * as React from 'react';
import {DefinitionWidget} from "../widgets/definition";
import {get_title_formatting} from "../util";
import {faChevronRight} from "@fortawesome/free-solid-svg-icons/faChevronRight";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {request} from "../request";
import {faFlag} from "@fortawesome/free-solid-svg-icons/faFlag";
import {ReportWidget} from "../widgets/report/report";

interface GuessDefinitionProps {
    word: Word,
    definitions: Array<[number, string, number]>,
    mode: string,
    next: () => void,
    setPoints: (x: number) => void,
}

interface GuessDefinitionState {
    freeze: boolean;
    correct: undefined | number;
    guess: undefined | number;
    total_guesses: undefined | number;
    reporting?: boolean
}

export class GuessDefinition extends React.PureComponent<GuessDefinitionProps, GuessDefinitionState> {
    constructor(props: GuessDefinitionProps) {
        super(props);

        this.state = {
            freeze: false,
            correct: undefined,
            guess: undefined,
            total_guesses: undefined
        }
    }

    onCardClick(card: number): void {
        if (this.state.freeze) {
            return;
        }

        this.setState({
            freeze: true,
            guess: card
        });

        request(
            '/api/submit_guess',
            {
                word: this.props.word.word,
                definition: card
            }
        ).then(
            (data) => {
                this.props.setPoints(
                    data.new_player_points
                );
                this.setState({
                        correct: data.correct_answer,
                        total_guesses: data.total_guesses
                    }
                )
            }
        ).catch(
            (err) => {
                console.error(err);
            }
        )
    }

    render() {
        return <div>
            <h1>{get_title_formatting(this.props.word.pos, 'guess', this.props.word.word)}</h1>
            <div className={"definitions-container"}>
                {
                    this.props.definitions.map(
                        (definition) => (
                            <DefinitionWidget
                                key={definition[0]}
                                word={this.props.word}
                                definition={definition[1]}
                                onClick={() => this.onCardClick(
                                    definition[0]
                                )}
                                percentage={this.state.correct && this.state.freeze ?
                                    100 * (definition[2] + (
                                        this.state.guess === definition[0] ? 1 : 0
                                    )) / this.state.total_guesses : 0
                                }
                                correct={
                                    this.state.correct === definition[0] ?
                                        true :
                                        this.state.guess === definition[0] && this.state.correct !== undefined ?
                                            false : null}
                            />
                        ))
                }
            </div>

            <div className="button-row">
                <a href="#"
                   onClick={(ev) => {
                       ev.preventDefault();
                       this.props.next();
                   }}
                >
                    Skip
                </a>
                <button
                    disabled={this.state.correct === undefined}
                    onClick={this.props.next}
                >Next<FontAwesomeIcon
                    icon={faChevronRight}
                />
                </button>
            </div>

            <button className="report-button"
                    onClick={() => this.setState({reporting: true})}
            >
                <FontAwesomeIcon icon={faFlag}/>
                Report
            </button>

            <p
                className='fading-p'
            >You will gain 10 points if you guess correctly.</p>

            {
                this.state.reporting &&
                <ReportWidget
                  definitions={this.props.definitions}
                  close={() => this.setState({
                      reporting: false
                  })}

                />
            }
        </div>
    }
}