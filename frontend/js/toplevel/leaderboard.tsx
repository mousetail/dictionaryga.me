import * as React from 'react';
import {request} from "../request";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSync} from "@fortawesome/free-solid-svg-icons/faSync";
import {SocialButtons} from "../socialButtons";

interface LeaderboardProps {
    onPageChange: (page: string) => void;
    user: User
}

interface LeaderboardState {
    loading: boolean,
    error?: string,
    scores?: { username: string, score: number }[];
    order: string
}

export class Leaderboard extends React.PureComponent<LeaderboardProps, LeaderboardState> {
    constructor(props: LeaderboardProps) {
        super(props);

        this.state = {
            loading: true,
            error: undefined,
            order: "day"
        };
        this.refresh = this.refresh.bind(this);
    }

    componentDidMount(): void {
        this.refresh();
    }

    refresh(order?: any) {
        if (typeof order !== "string") {
            order = this.state.order;
        }
        this.setState({
            loading: true
        });
        request(
            '/api/leaderboard/' + order
        ).then(
            data => {
                this.setState({
                    scores: data.highscores,
                    loading: false
                })
            }
        ).catch(
            (error) => {
                this.setState({
                    error: "" + error
                })
            }
        )
    }

    setOrder(order: string) {
        this.setState({
            order: order
        });
        this.refresh(order);
    }

    render() {

        let leaderboard_main;
        if (this.state.loading) {

            leaderboard_main = <div className="leaderboard">
                Loading...
                {this.state.error}
            </div>
        } else {
            leaderboard_main = <div className="leaderboard">
                {
                    this.state.scores.map(score => {
                        return <>
                            <div className={"leader-cell"} key={score.username}>
                                {score.username}
                            </div>
                            <div className={"leader-score"} key={"score-of-" + score.username}>
                                {score.score}
                            </div>
                        </>
                    })
                }
            </div>
        }

        return <div>
            <h1>Leaderboard</h1>

            <div className={"leaderboard-select"}>
                <a href="#"
                   onClick={() => this.setOrder('day')}
                   className={this.state.order === 'day' ? 'leaderboard-select-active' : ''}
                >
                    Today
                </a>
                <a href="#"
                   onClick={() => this.setOrder('week')}
                   className={this.state.order === 'week' ? 'leaderboard-select-active' : ''}>
                    This week
                </a>
                <a href="#"
                   className={this.state.order === 'month' ? 'leaderboard-select-active' : ''}
                   onClick={() => this.setOrder('month')}
                >
                    This month
                </a>
                <a href="#"
                   className={this.state.order === 'all' ? 'leaderboard-select-active' : ''}
                   onClick={() => this.setOrder('all')}>
                    All Time
                </a>
            </div>

            {leaderboard_main}

            {!this.props.user.hasAccount && <p className={"fading-p"}>
              You must register in order to appear on the leaderboard.
              <a href="#"
                 onClick={(ev) => {
                     ev.preventDefault();
                     this.props.onPageChange('account')
                 }}>
                Register now.
              </a>
            </p>}

            <SocialButtons fixed={false}/>

            <div className="button-row">
                <button
                    onClick={this.refresh}
                >
                    <FontAwesomeIcon
                        icon={faSync}
                    />
                    Refresh
                </button>
                <button
                    onClick={() => this.props.onPageChange('')}
                >
                    Back to game
                </button>
            </div>
        </div>
    }
}