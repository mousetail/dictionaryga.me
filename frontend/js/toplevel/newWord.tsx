import * as React from 'react';
import {get_cookie} from "../util";
import {request} from "../request";
import {ErrorWidget} from "../widgets/error";

interface NewWordProps {
    onPageChange: (p: string) => void;
}

interface NewWordState {
    word: string,
    previous_word: undefined | string;
    definition: string,
    freeze: boolean,
    error?: string
}

export class NewWord extends React.PureComponent<NewWordProps, NewWordState> {
    constructor(props: NewWordProps) {
        super(props);

        this.state = {
            word: '',
            definition: '',
            freeze: false,
            previous_word: undefined

        }
    }

    submit() {
        if (this.state.freeze) {
            return;
        }

        this.setState({
            freeze: true
        });

        request('/api/new_word',
            {
                word: this.state.word,
                definition: this.state.definition
            }
        ).then(
            (res) => {
                this.setState({
                    previous_word: this.state.word,
                    word: "",
                    definition: ""
                })
            }
        ).catch(
            (err) => {
                this.setState({error: "" + err});
            }
        )
    }

    render() {
        return (
            <div>
                <h1>Submit a new word</h1>
                <p className={"fading-p"}>
                    Have you come across a cool word recently you think few people know about?
                </p>
                <ErrorWidget error={this.state.error}/>
                <div className={"form"}>
                    {this.state.previous_word && 'Thank you for submitting ' + this.state.previous_word}
                    <label>Word:
                        <input
                            value={this.state.word}
                            onChange={(ev) => this.setState({
                                'word': ev.target.value
                            })}
                            placeholder={"Pick a word"}
                        />
                    </label>
                    <label>Definition:
                        <textarea
                            value={this.state.definition}
                            onChange={(ev) => this.setState({
                                'definition': ev.target.value
                            })}
                            placeholder={"Enter the official definition of the word"}
                        />
                    </label>
                </div>
                <a href='#'
                   className={'submit-button'}
                   onClick={(ev) => {
                       ev.preventDefault();
                       this.props.onPageChange('')
                   }}>Back to game</a>
                <button onClick={this.submit.bind(this)} className="submit-button">Submit</button>
                <p className={"fading-p"}>
                    You will gain points if people guess incorrectly
                </p>
            </div>
        );
    }
}