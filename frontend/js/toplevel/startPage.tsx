import * as React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFacebookSquare} from "@fortawesome/free-brands-svg-icons/faFacebookSquare";
import {faReddit} from "@fortawesome/free-brands-svg-icons/faReddit";
import {SocialButtons} from "../socialButtons";

interface StartPageProps {
    onStart: () => void,
    onPageChange: (x: string) => void
}

interface StartPageState {

}

export class StartPage extends React.PureComponent<StartPageProps, StartPageState> {
    render() {
        return <div className="vertical-flex">
            <h1>The Dictionary Game</h1>

            <p className="fading-p">
                The game about strange and unusual english words.
            </p>

            <p className="fading-p">
                In dictionarygame, playes guess the correct definitions of words, or write new definitions
                for other to guess.
            </p>

            <div className={"space-filler"}/>

            <div className="two-columns">
                <div>
                    <h2>
                        Guess words
                    </h2>

                    <div className="definitions-container front-page-definitions-container">
                        {/*<div>Does <b>Chartreuse</b> mean</div>*/}
                        <div className="definition">
                            <b className="word">Chartreuse</b><span className={"pos"}>[N]</span>
                            A color between green and yellow
                        </div>

                        {/*<div>Or does it mean</div>*/}
                        <div className="definition">
                            <b className="word">Chartreuse</b><span className={"pos"}>[N]</span>
                            (1764 - 1801) French Architect, famous for the Plaza de la Casa in Milan.
                        </div>
                    </div>

                    <p className="fading-p">
                        Guess the right definition of strange unknown words.
                    </p>


                </div>
                <div>

                    <h2>
                        Write new definitions
                    </h2>

                    <div className={"definition definition-edit"}>
                        <label>
                            <b>Chartreuse</b>
                            <span className={'pos'}>[N]</span>
                            <textarea
                                disabled={true}
                                placeholder={'enter definition here'}
                            />
                        </label>
                    </div>

                    <p className="fading-p">
                        Attempt to trick people into guessing your words.
                    </p>
                </div>
            </div>


            <div className="space-filler">
            </div>


            <button className={'submit-button extra-big-button'} onClick={this.props.onStart}>Start!</button>
            <div className={"button-row"}>
                <button
                    onClick={(ev) => {
                        ev.preventDefault();
                        this.props.onPageChange('login')
                    }}
                >Log in
                </button>
                <button
                    onClick={() => this.props.onPageChange('about')}
                >
                    About
                </button>
            </div>


            <SocialButtons fixed={true}/>
        </div>
    }
}