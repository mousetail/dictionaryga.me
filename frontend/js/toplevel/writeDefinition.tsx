import * as React from 'react';
import {get_title_formatting} from "../util";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronRight} from "@fortawesome/free-solid-svg-icons/faChevronRight";
import {request} from "../request";
import {ErrorWidget} from "../widgets/error";

interface WriteDefinitionProps {
    mode: string,
    word: Word,
    next: () => void,
    setPoints: (i: number) => void
}

interface WriteDefinitionState {
    definition: string,
    freeze: boolean,
    error: undefined | string,
}

export class WriteDefinition extends React.PureComponent<WriteDefinitionProps, WriteDefinitionState> {
    constructor(props: WriteDefinitionProps) {
        super(props);

        this.state = {
            definition: '',
            freeze: false,
            error: undefined
        }
    }

    submit() {
        if (this.state.freeze) {
            return
        }
        this.setState({
            freeze: true
        });

        request('/api/submit_definition',
            {
                word: this.props.word.word,
                definition: this.state.definition
            }
        ).then(
            () => this.props.next()
        ).catch(
            (err) => {
                this.setState({
                    error: "" + err,
                    freeze: false
                })
            }
        )
    }

    render() {
        return <div>
            <h1>{get_title_formatting(this.props.word.pos, 'write', this.props.word.word)}</h1>

            <p className="fading-p">
                If you know the correct answer, don't enter it. Try to be creative.
            </p>

            <ErrorWidget error={this.state.error}/>

            <div className={"definition definition-edit"}>
                <label>
                    <b>{this.props.word.word}</b>
                    <span className={'pos'}>[{this.props.word.pos}]</span>
                    <textarea value={this.state.definition}
                              disabled={this.state.freeze}
                              placeholder={'enter definition here'}
                              onChange={(ev) => {
                                  this.setState({
                                      'definition':
                                      ev.target.value
                                  });
                              }}
                    />
                </label>
            </div>

            <div className="button-row">
                <a href={"#"}
                   onClick={this.props.next}>
                    Skip
                </a>
                <button
                    onClick={this.submit.bind(this)}
                >
                    Submit <FontAwesomeIcon
                    icon={faChevronRight}
                />
                </button>
            </div>
            <p
                className='fading-p'
            >You will gain 5 points for every guess on your definition.</p>
        </div>
    }
}