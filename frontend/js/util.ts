export function get_cookie(name: string): string {
    let value = "; " + document.cookie;
    let parts = value.split("; " + name + "=");
    if (parts.length == 2) {
        return parts.pop().split(";").shift();
    }
}

export function format_number(number: number) {
    return (number + '').replace(/(\..*)$|(\d)(?=(\d{3})+(?!\d))/g, (digit, fract) => fract || digit + ',');
}

export function get_title_formatting(pos: 'N' | 'V' | 'ADJ' | 'ADV', mode: 'guess' | 'write', word: string) {
    let second_part: string;
    switch (pos) {
        case 'N':
            second_part = 'is a';
            break;
        case 'V':
            second_part = 'it means to';
            break;
        case 'ADJ':
            second_part = 'it means for something to be';
            break;
        case 'ADV':
            second_part = 'is means to do something';
            break;
        default:
            second_part = 'is a';
    }

    let first_part: string;
    switch (mode) {
        case "guess":
            first_part = 'What do you think';
            break;
        case "write":
            first_part = 'Plausibly, what could';
            if (second_part == "is a") {
                second_part = "be a"
            }
            break;
        default:
            first_part = 'I don\t know what';
    }

    return first_part + ' ' + second_part + ' \"' + word + '\"?'
}