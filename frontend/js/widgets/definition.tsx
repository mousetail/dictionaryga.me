import * as React from 'react';
import {faCheckCircle} from "@fortawesome/free-solid-svg-icons/faCheckCircle";
import {faTimesCircle} from "@fortawesome/free-solid-svg-icons/faTimesCircle";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

interface DefinitionWidgetProps {
    word: Word,
    definition: string;
    onClick: (x: React.MouseEvent<HTMLElement>) => void;
    correct: null | boolean;
    percentage: number;
}

interface DefinitionWidgetState {

}

export class DefinitionWidget extends React.PureComponent<DefinitionWidgetProps, DefinitionWidgetState> {
    constructor(props: DefinitionWidgetProps) {
        super(props);
    }

    render() {
        return (
            <div onClick={this.props.onClick} className="definition">
                {
                    this.props.correct === null ? null :
                        this.props.correct ? <FontAwesomeIcon
                                icon={faCheckCircle}
                                className='definition-icon definition-icon-correct'/> :
                            <FontAwesomeIcon
                                icon={faTimesCircle}
                                className='definition-icon definition-icon-incorrect'/>
                }
                <b className={'word'}>{this.props.word.word}</b>
                <span className={'pos'}>[{this.props.word.pos}]</span>
                <span>{this.props.definition}</span>

                <div className={"progress-bar"}>
                    <div style={{width: this.props.percentage + '%'}}/>
                </div>
            </div>
        );
    }
}

