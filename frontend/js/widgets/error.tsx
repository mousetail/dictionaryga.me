import * as React from "react";
import {faExclamationTriangle} from "@fortawesome/free-solid-svg-icons/faExclamationTriangle";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

interface ErrorProps {
    error: Error | string | undefined;
}

interface ErrorState {

}

export class ErrorWidget extends React.PureComponent<ErrorProps, ErrorState> {
    render(): JSX.Element {
        if (this.props.error === undefined) {
            return null
        }
        return <div className="error">
            <FontAwesomeIcon
                icon={faExclamationTriangle}/>
            {"" + this.props.error}
        </div>
    }
}