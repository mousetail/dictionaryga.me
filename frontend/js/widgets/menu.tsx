import * as React from 'react';
import {faChartBar} from "@fortawesome/free-solid-svg-icons/faChartBar";
import {faLifeRing} from "@fortawesome/free-solid-svg-icons/faLifeRing";
import {faStar} from "@fortawesome/free-solid-svg-icons/faStar";
import {faUser} from "@fortawesome/free-solid-svg-icons/faUser";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {IconDefinition} from '@fortawesome/fontawesome-svg-core';

interface MenuProps {
    onPageChange: (x: string) => void;
}

interface MenuState {

}

interface MenuItem {
    icon: IconDefinition,
    name: string,
    page: string
}

const menuItems: MenuItem[] = [
    {
        'icon': faChartBar,
        'name': 'Leaderboard',
        'page': 'leaderboard'
    },
    {
        'icon': faUser,
        'name': 'Account',
        'page': 'account'
    },
    {
        'icon': faStar,
        'name': 'Submit word',
        'page': 'new'
    },
    {
        'icon': faLifeRing,
        'name': 'Help',
        'page': 'about'
    }
];

export class Menu extends React.PureComponent<MenuProps, MenuState> {
    render(): JSX.Element {
        return <div className={"menu"}>
            {
                menuItems.map(item => {
                    return <div
                        onClick={() => this.props.onPageChange(item.page)}
                        key={item.name}
                    >
                        <FontAwesomeIcon icon={item.icon}/>
                        {item.name}
                    </div>
                })
            }
        </div>
    }
}