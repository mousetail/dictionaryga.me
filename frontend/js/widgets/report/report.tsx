import * as React from 'react';
import {IconDefinition} from "@fortawesome/fontawesome-svg-core";
import {faCopy} from "@fortawesome/free-solid-svg-icons/faCopy";
import {faCheckDouble} from "@fortawesome/free-solid-svg-icons/faCheckDouble";
import {faSkull} from "@fortawesome/free-solid-svg-icons/faSkull";
import {faQuestionCircle} from "@fortawesome/free-solid-svg-icons/faQuestionCircle";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheckSquare} from "@fortawesome/free-solid-svg-icons/faCheckSquare";
import {faSquare} from "@fortawesome/free-solid-svg-icons/faSquare";
import {request} from "../../request";

interface ReportProps {
    definitions: Array<[number, string, number]>,
    close: () => void
}

interface ReportState {
    reason?: undefined | ReportReason
    page: number,
    selectedAnswers: boolean[];
    loading: boolean,
    error: string
}

interface ReportReason {
    name: string,
    toName: string,
    description: string,
    icon: IconDefinition,
    nrofOptions: [number, number]
}

const reportReasons: ReportReason[] = [
    {
        name: 'Duplicate Answers',
        toName: 'duplicate',
        description: 'Two or more answers are the same or have the same basic meaning',
        icon: faCopy,
        nrofOptions: [2, 2]
    },
    {
        name: 'Multiple Correct Answers',
        toName: 'correct',
        description: 'A "incorrect" answer is actually also a different correct' +
            ' definition for the word.',
        icon: faCheckDouble,
        nrofOptions: [1, 100]
    },
    {
        name: 'Spam',
        toName: 'spam',
        description: 'One answer is not a definition but promotes some service, is a joke unrelated' +
            ' to the word, or is unintelligible.',
        icon: faSkull,
        nrofOptions: [1, 1]
    },
    {
        name: 'Other',
        toName: 'problematic',
        description: 'This answer needs moderator attention for a different reason',
        icon: faQuestionCircle,
        nrofOptions: [1, 1]
    }
];

export class ReportWidget extends React.PureComponent<ReportProps, ReportState> {
    constructor(props: ReportProps) {
        super(props);
        this.state = {
            page: 1,
            selectedAnswers: props.definitions.map(i => false),
            loading: false,
            error: undefined
        };

        this.submit = this.submit.bind(this);
    }

    submit() {
        if (this.state.loading) {
            return;
        }

        let nrof_selected = this.state.selectedAnswers.reduce(
            (prev, cur) =>
                cur ? prev + 1 : prev, 0);
        if (this.state.reason.nrofOptions[0] > nrof_selected ||
            this.state.reason.nrofOptions[1] < nrof_selected) {
            if (this.state.reason.nrofOptions[0] == this.state.reason.nrofOptions[1]) {
                this.setState({
                    error: "Must select exactly " + this.state.reason.nrofOptions[0] + " options"
                });
            } else {
                this.setState({
                    error: "Must select between " +
                        this.state.reason.nrofOptions[0]
                        + " and " +
                        this.state.reason.nrofOptions[1]
                        + " definitions"
                })
            }
            return;
        }

        this.setState({
            loading: true
        });

        request(
            "/api/report",
            {
                "reason": this.state.reason.toName,
                "answers": this.props.definitions.filter(
                    (def, index) => this.state.selectedAnswers[index]
                ).map(
                    def => def[0]
                )
            }
        ).then(
            (res) => {
                this.setState({
                    loading: false,
                    error: undefined,
                    page: 3
                })
            }
        ).catch(
            err => {
                this.setState({
                        error: err,
                        loading: false
                    }
                )
            }
        )
    }

    startPage() {
        return <><h2>Please select a reason for the report</h2>

            {reportReasons.map((reason) => {
                return <div className={"report-reason"}
                            key={reason.toName}
                            onClick={() => {
                                this.setState({
                                    page: 2,
                                    reason: reason
                                })
                            }}
                >
                    <FontAwesomeIcon icon={reason.icon}/>
                    <span className="report-heading">
                            {reason.name}
                    </span>
                    <span>
                            {reason.description}
                    </span>
                </div>
            })}
        </>

    }

    selectAnswerPage() {
        return <>
            <h2>Please select the answer(s) to report as {this.state.reason.toName}</h2>

            <div>
                {"" + this.state.error}
            </div>
            {
                this.props.definitions.map(
                    ([id, definition, guesses], index) => {
                        return <div
                            key={id}
                            className="report-reason"
                            onClick={() => {
                                this.setState(state => ({
                                    selectedAnswers: state.selectedAnswers.map(
                                        (i, index2) =>
                                            index === index2 ? !i : i
                                    )
                                }))
                            }}
                        >
                            {
                                this.state.selectedAnswers[index] ?
                                    <FontAwesomeIcon icon={faCheckSquare}/> :
                                    <FontAwesomeIcon icon={faSquare}/>
                            }

                            {definition}
                        </div>
                    }
                )
            }
            <button
                onClick={this.submit}
            >Submit
            </button>
        </>
    }

    renderThankYou() {
        return <>
            <p>Thank you for your report!</p>
            <button onClick={this.props.close}>
                Close window
            </button>
        </>
    }

    render() {
        let content: JSX.Element;

        switch (this.state.page) {
            case 1:
                content = this.startPage();
                break;
            case 2:
                content = this.selectAnswerPage();
                break;
            case 3:
                content = this.renderThankYou();
        }

        return <div className={"report-modal-container"}
                    onClick={this.props.close}
        >
            <div className={"report-modal"}
                 onClick={(ev) => ev.stopPropagation()}
            >
                {content}
            </div>
        </div>
    }
}

