import * as React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBars} from "@fortawesome/free-solid-svg-icons/faBars";

interface ScoreProps {
    score: number;
    activateMenu: () => void
}

interface ScoreState {
    displayScore: number;
}

export class Score extends React.PureComponent<ScoreProps, ScoreState> {
    constructor(props: ScoreProps) {
        super(props);

        this.state = {
            displayScore: props.score
        };

        this.updateScore = this.updateScore.bind(this);
    }

    updateScore() {
        this.setState({
            displayScore: this.props.score
        });
    }

    render() {
        return <div className='score'
                    onClick={() => this.props.activateMenu()}
        >
            <FontAwesomeIcon
                icon={faBars}
            />
            {this.state.displayScore}
            {
                this.state.displayScore != this.props.score &&
                <div className='score-inner'
                     onAnimationEnd={this.updateScore}
                >
                    {this.props.score - this.state.displayScore}
                </div>
            }
        </div>
    }
}