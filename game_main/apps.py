from django.apps import AppConfig


class GameMainConfig(AppConfig):
    name = 'game_main'
