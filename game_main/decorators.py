import json
import functools

from django.http.response import HttpResponseBadRequest, HttpResponseNotAllowed
from . import models


def json_call(funk):
    @functools.wraps(funk)
    def _json_call(request, *args, **kwargs):
        try:
            data = json.loads(request.body)
        except json.JSONDecodeError:
            return HttpResponseBadRequest("Invalid json")

        return funk(request, *args, **kwargs, data=data)

    return _json_call


def requires_player(funk):
    @functools.wraps(funk)
    def _requires_player(request, *args, **kwargs):
        try:
            player = models.Player.objects.get(
                id=request.session['uu_id']
            )
        except models.Player.DoesNotExist:
            return HttpResponseNotAllowed("Create authentication key first")

        return funk(request, *args, **kwargs, player=player)

    return _requires_player
