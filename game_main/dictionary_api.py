import json

from django.conf import settings
import requests

part_of_speech_translator = {
    "noun": "n",
    "adjective": "adj",
    "adverb": "adv",
    "verb": "v",
    "geographical name": "n",
    "abbreviation": "n",
    "biographical name": "n",
    "Italian adverb": "adv",
    "French phrase": "n",
    "Latin phrase": "n",
    'adverb or adjective': 'adj',
    'adjective or adverb': 'adj',
    'idiom': 'n',
    'plural noun': 'n'
}


class GetWordException(ValueError):
    pass


def get_word_info(word):
    response = requests.get(
        'https://dictionaryapi.com/api/v3/references/collegiate/json/{}'.format(
            word
        ),
        params={'key': settings.DICTIONARY_API_KEY}
    )

    if response.status_code == 404:
        raise GetWordException("This word does not appear in our dictionary")

    word_data = response.json()[0]
    if isinstance(word_data, str):
        raise GetWordException("A synonym for this word already exists in our dictionary")
        # return get_word_info(word_data)
    while isinstance(word_data, list):
        word_data = word_data[0]

    if not isinstance(word_data, dict):
        raise GetWordException("API returned an invalid value")

    if "prs" not in word_data["hwi"] or len(word_data["hwi"]["prs"]) == 0:
        pronunciation = None
    else:
        pronunciation = word_data["hwi"]["prs"][0]["mw"]

    print(word_data["shortdef"])

    if len(word_data["shortdef"]) == 0:
        raise GetWordException("No official definition found for this word")

    return {
        "definition": word_data["shortdef"][0],
        "pronunciation": pronunciation,
        "part_of_speech": part_of_speech_translator[word_data["fl"].split(',')[0]].upper()
    }
