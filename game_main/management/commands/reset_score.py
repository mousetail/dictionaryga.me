from django.utils.datetime_safe import datetime
from django.core.management.base import BaseCommand, CommandError
from django.db import connection
from game_main import models


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        date = datetime.now()
        if date.hour == 0:
            print("resetting day")
            models.Player.objects.all().update(points_day=0)
        if date.weekday() == 0 and date.hour == 0:
            print("resetting week")
            models.Player.objects.all().update(points_week=0)
        if date.day == 1 and date.hour == 0:
            print("resetting month")
            models.Player.objects.all().update(points_month=0)
        if date.day == 1 and date.month == 1 and date.hour == 0:
            print("resetting year")
            models.Player.objects.all().update(points_year=0)
        return
