# Generated by Django 3.0.2 on 2020-01-20 19:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('game_main', '0002_auto_20200120_1243'),
    ]

    operations = [
        migrations.AddField(
            model_name='word',
            name='part_of_speech',
            field=models.CharField(choices=[('N', 'Noun'), ('V', 'Verb'), ('ADJ', 'Adjective'), ('ADV', 'Adverb')], default='N', max_length=8),
        ),
    ]
