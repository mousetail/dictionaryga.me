import uuid

from django.db import models
from django.contrib.auth import models as auth_models


# Create your models here.
class Player(models.Model):
    user = models.ForeignKey(
        auth_models.User,
        null=True,
        default=None,
        on_delete=models.CASCADE
    )
    code = models.UUIDField(
        default=uuid.uuid4,
        db_index=True
    )
    points = models.IntegerField(default=0)
    points_day = models.IntegerField(default=0)
    points_week = models.IntegerField(default=0)
    points_month = models.IntegerField(default=0)
    points_year = models.IntegerField(default=0)
    most_recent_word = models.ForeignKey('game_main.Word', default=None, null=True, on_delete=models.SET_NULL)

    def add_points(self, value: int):
        self.points += value
        self.points_day += value
        self.points_month += value
        self.points_year += value
        self.points_week += value


class Word(models.Model):
    word = models.CharField(primary_key=True, max_length=64)
    correct_definition = models.ForeignKey(to='game_main.Definition', on_delete=models.CASCADE,
                                           null=True, related_name='+')
    correct_guesses = models.IntegerField(default=0)
    total_guesses = models.IntegerField(default=0)

    PART_OF_SPEECH_CHOICES = (
        ('N', 'Noun'),
        ('V', 'Verb'),
        ('ADJ', 'Adjective'),
        ('ADV', 'Adverb')
    )

    nrof_answers = models.IntegerField(default=1)

    pronunciation = models.CharField(
        max_length=64,
        default='þro/néu/nci/ɐti/ón',
        null=True
    )

    part_of_speech = models.CharField(
        max_length=8,
        choices=PART_OF_SPEECH_CHOICES,
        default='N'
    )
    author = models.ForeignKey(
        Player,
        on_delete=models.SET_NULL,
        null=True
    )


class Definition(models.Model):
    word = models.ForeignKey(to=Word, on_delete=models.CASCADE, null=False,
                             related_name="definitions")
    definition = models.TextField()
    guesses = models.IntegerField(default=0)
    author = models.ForeignKey(
        Player,
        on_delete=models.SET_NULL,
        null=True
    )
    hidden = models.BooleanField(
        default=False
    )


class Report(models.Model):
    word = models.ForeignKey(to=Word, on_delete=models.CASCADE,
                             db_index=True)
    definition_1 = models.ForeignKey(to=Definition, on_delete=models.CASCADE,
                                     related_name='+')
    definition_2 = models.ForeignKey(to=Definition, on_delete=models.CASCADE,
                                     null=True, related_name='+')
    reason = models.CharField(max_length=32)
    author_1 = models.ForeignKey(to=Player, on_delete=models.SET_NULL, null=True,
                                 related_name='+')
    author_2 = models.ForeignKey(to=Player, on_delete=models.SET_NULL, null=True,
                                 related_name='+')
    author_3 = models.ForeignKey(to=Player, on_delete=models.SET_NULL, null=True,
                                 related_name='+')
    submissions = models.IntegerField(default=1)
