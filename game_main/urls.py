from django.urls import path
from . import views

urlpatterns = [
    path(r'api/start_game', views.start_game),

    path(r'api/register', views.create_account),
    path(r'api/login', views.log_in),

    path(r'api/leaderboard/<str:score>', views.leaderboard),
    path(r'api/word', views.get_word),
    path(r'api/submit_guess', views.submit_word),
    path(r'api/submit_definition', views.submit_definition),
    path(r'api/new_word', views.submit_new_word),
    path(r'api/report', views.report),
    path(r'', views.home),
    path(r'favicon.ico', views.favicon)
]
