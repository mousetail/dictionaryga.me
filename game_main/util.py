import random


def choice_or_none(collection):
    if len(collection) == 0:
        return None
    return random.choice(
        collection
    )


def edit_distance(word1, word2):
    # From https://stackoverflow.com/questions/2460177/edit-distance-in-python
    # A comment states this algorithm is incorrect, but it seems to show the correct values
    m = len(word1) + 1
    n = len(word2) + 1

    tbl = {}
    for i in range(m):
        tbl[i, 0] = i
    for j in range(n):
        tbl[0, j] = j
    for i in range(1, m):
        for j in range(1, n):
            cost = 0 if word1[i - 1] == word2[j - 1] else 1
            tbl[i, j] = min(
                tbl[i, j - 1] + 1,
                tbl[i - 1, j] + 1,
                tbl[i - 1, j - 1] + cost)

    return tbl[m - 1, n - 1]


if __name__ == "__main__":
    words = [
        ('apple', 'Apple'),
        ('apple', 'appple'),
        ('apple', 'appples'),
        ('the fine apple', 'the apple'),
        ('Big plate of fungus', 'Big fungus plate'),
    ]

    for w1, w2 in words:
        print("{:>15} {:>15} {: 9}".format(w1, w2, edit_distance(w1, w2)))
