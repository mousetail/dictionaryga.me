import typing

from django.core.exceptions import ValidationError
from django.db import IntegrityError
from django.shortcuts import render
from django.http.response import JsonResponse, HttpResponseBadRequest, HttpResponseRedirect
from django.views.decorators.http import require_POST
from django.contrib.auth import models as auth_models
from django.contrib.auth import password_validation, authenticate, login
from django.views.decorators.csrf import ensure_csrf_cookie
import random

from . import models, util

# Create your views here.
from .decorators import requires_player, json_call
from .dictionary_api import get_word_info, GetWordException


@ensure_csrf_cookie
def home(request):
    return render(request, 'home.html')


def favicon(request):
    return HttpResponseRedirect(
        '/static/images/icon.png'
    )


def get_word_with_no_answers(last_10_words: list):
    return 'write', util.choice_or_none(
        models.Word.objects.all().filter(nrof_answers__lte=1
                                         ).exclude(
            word__in=last_10_words
        )
    )


def get_word_with_incomplete_answers(last_10_words: list):
    return 'write', util.choice_or_none(
        models.Word.objects.filter(nrof_answers__range=(2, 5)).exclude(
            word__in=last_10_words
        )
    )


def get_word_with_sufficient_answers(last_10_words: list):
    return 'guess', util.choice_or_none(
        models.Word.objects.filter(nrof_answers__gte=4).exclude(
            word__in=last_10_words
        )
    )


def get_word_multiple_sources(last_10_words, *sources: typing.Callable[[list], typing.Tuple[str, str]]):
    mode, word = None, None
    for source in sources:
        print("attempting source ", source)
        if word is None:
            mode, word = source(last_10_words)
        else:
            break
    return mode, word


@requires_player
def get_word(request, player):
    request.session["q_num"] = request.session.get("q_num", 0) + 1

    last_10_words = request.session.get("last_10_words", [])

    # 10/15 Chance: Guess a word with min 4 definitions
    #  4/15 Chance: Add a definition to a word with 1-5 definitions
    #  1/15 Chance: Add definition for new word with 0 definitions
    # If one option is unavailable, try another
    # Fall through if none ar available
    # This should guarantee throughput of new word

    word = None
    mode = None
    if (request.session["q_num"]) % 4 == 0:
        if random.randrange(0, 3) == 0:
            print(f"\tNew word chosen by chance {request.session['q_num']}")
            mode, word = get_word_multiple_sources(last_10_words,
                                                   get_word_with_no_answers,
                                                   get_word_with_incomplete_answers,
                                                   get_word_with_sufficient_answers)
            # word = get_word_with_no_answers(last_10_words)
        if word is None:
            print(f"\tOld word chosen since new word can't be found {request.session['q_num']}")
            mode, word = get_word_multiple_sources(last_10_words,
                                                   get_word_with_incomplete_answers,
                                                   get_word_with_no_answers,
                                                   get_word_with_sufficient_answers)
            # word = get_word_with_incomplete_answers(last_10_words)
    if word is None:
        print(f"Creating guess since we can't find a existing one {request.session['q_num']}")
        # mode = 'guess'
        # word = get_word_with_sufficient_answers(last_10_words)

        mode, word = get_word_multiple_sources(last_10_words,
                                               get_word_with_sufficient_answers,
                                               get_word_with_incomplete_answers,
                                               get_word_with_no_answers)
    # If all else fails
    if word is None:
        mode = 'write'
        word = util.choice_or_none(
            models.Word.objects.all().exclude(
                word__in=last_10_words
            )
        )
    definitions = [
        (i.id, i.definition[0].lower() + i.definition[1:], i.guesses) for i in word.definitions.filter(hidden=False)[:6]
    ]

    last_10_words.append(word.word)
    if len(last_10_words) > 10:
        last_10_words = last_10_words[-10:]
    request.session["last_10_words"] = last_10_words

    if len(definitions) >= 4:
        random.shuffle(definitions)

    player.most_recent_word = word
    player.save()

    return JsonResponse(
        {
            'word': {
                'word': word.word,
                'pos': word.part_of_speech.upper(),
                'pronunciation': word.pronunciation
            },
            'mode': mode,
            'definitions': definitions,
            'last_10_words': request.session["last_10_words"],
            'word_index': request.session['q_num']
        }
    )


@require_POST
@json_call
def start_game(request, data):
    uid = data.get('uid', None)
    player = None
    if uid is not None:
        try:
            player = models.Player.objects.get(code=uid)
        except models.Player.DoesNotExist:
            uid = None
        else:
            request.session['uu_id'] = player.id
            request.session['uid'] = str(uid)
    if uid is None:
        player = models.Player()
        player.save()

        request.session['uu_id'] = player.id
        request.session['uid'] = str(player.code)
        request.session["q_num"] = 0
        uid = str(player.code)

    return JsonResponse({
        'user': {
            'uid': uid,
            'points': player.points,
            'hasAccount': player.user is not None,
            'username': (player.user and player.user.username) or None
        },
        'uid': uid,
        'new_player_points': player.points
    })


@require_POST
@json_call
@requires_player
def submit_word(request, data, player):
    word = models.Word.objects.get(word__iexact=data["word"])
    definition = models.Definition.objects.get(id=data["definition"])

    if (player.most_recent_word != word):
        return HttpResponseBadRequest("Not the last question")

    player.most_recent_word = None
    player.save()

    if word.correct_definition_id == definition.id:
        word.correct_guesses += 1
        player.add_points(10)
        player.save()
        correct = True
    else:
        if definition.author is not None:
            definition.author.add_points(5)
            definition.author.save()
        correct = False
    word.total_guesses += 1
    definition.guesses += 1
    word.save()
    definition.save()

    return JsonResponse({
        "correct": correct,
        "correct_answer": word.correct_definition_id,
        "correct_guesses": word.correct_guesses,
        "total_guesses": word.total_guesses,
        "new_player_points": player.points
    })


@require_POST
@json_call
@requires_player
def submit_definition(request, data, player):
    word = models.Word.objects.get(
        word__iexact=data["word"]
    )

    if player.most_recent_word != word:
        return HttpResponseBadRequest("Not the last question")
    player.most_recent_word = None
    player.save()

    definition_blacklist = [
        "no idea",
        "i don't know",
        "i do not know"
    ]

    if len(data["definition"]) == 0:
        return HttpResponseBadRequest("Definition may not be empty")
    if len(data["definition"]) <= 4:
        return HttpResponseBadRequest("Definition is too short")

    if data["definition"].lower() in definition_blacklist:
        return HttpResponseBadRequest("Make a guess! Be creative")

    data["definition"] = data["definition"][0].lower() + data["definition"][1:]

    for definition in word.definitions.all():
        if definition.definition.lower() == data["definition"].lower():
            return HttpResponseBadRequest("Definition should not be correct or too similar to any existing definition")
        distance = util.edit_distance(definition.definition.lower(), data["definition"].lower())
        if distance <= len(data["definition"]) // 15 + 4:
            return HttpResponseBadRequest("Definition should not be correct too similar to any existing definition " +
                                          "(difference: {} characters)".format(distance))

    definition = models.Definition(
        word=word,
        definition=data["definition"],
        author=player
    )
    definition.save()

    word.nrof_answers += 1
    word.save()

    return JsonResponse({
        'new_player_points': player.points
    })


@require_POST
@json_call
@requires_player
def submit_new_word(request, data, player):
    if (not isinstance(data["word"], str) or
            data["word"] == ''):
        return HttpResponseBadRequest("Invalid word")

    try:
        word_info = get_word_info(data["word"])
    except GetWordException as ex:
        return HttpResponseBadRequest(str(ex.args))

    try:
        word = models.Word(
            word=data["word"], author=player,
            pronunciation=word_info["pronunciation"],
            part_of_speech=word_info["part_of_speech"]
        )
        definition = models.Definition(
            word_id=data["word"],
            definition=word_info["definition"]
        )

        word.save()
        definition.save()
        word.correct_definition = definition
        word.save()
    except IntegrityError:
        return HttpResponseBadRequest("This word is already in the database")

    return JsonResponse({
        'new_player_points': player.points
    })


@require_POST
@json_call
@requires_player
def create_account(request, player, data):
    username = data["username"]
    password = data["password"]

    if auth_models.User.objects.filter(username=username).exists():
        return HttpResponseBadRequest("That username is already taken")

    try:
        password_validation.validate_password(password)
    except ValidationError as ex:
        return HttpResponseBadRequest(str(ex))

    user = auth_models.User.objects.create_user(
        username=username,
        password=password
    )
    user.save()
    player.user = user
    player.save()

    return JsonResponse({
        'user': {
            'points': player.points,
            'username': user.username,
            'hasAccount': True
        }
    })


@require_POST
@json_call
def log_in(request, data):
    username = data["username"]
    password = data["password"]

    user = authenticate(request, username=username, password=password)

    if user is not None:
        login(request, user)
    else:
        return HttpResponseBadRequest("Invalid username of password")

    player = user.player_set.get()
    request.session["uu_id"] = player.id
    request.session["uid"] = str(player.code)

    return JsonResponse({
        'uid': str(player.code),
        'new_player_points': player.points
    })


@requires_player
def leaderboard(request, score, player):
    if score == "all":
        order = "points"
    elif score == "day":
        order = "points_day"
    elif score == "week":
        order = "points_week"
    elif score == "month":
        order = "points_month"
    elif score == "year":
        order = "points-year"
    else:
        return HttpResponseBadRequest("order must be one of all, day, week, month or year")

    top_users = models.Player.objects.filter(
        user_id__isnull=False
    ).order_by("-" + order)[:10]

    users_data = [
        {
            "username": i.user.username,
            "score": getattr(i, order)
        }
        for i in top_users
    ]

    return JsonResponse({
        "highscores": users_data
    })


def handle_single_report(reason, answers, player):
    definitions = [models.Definition.objects.get(id=i) for i in answers]
    word = definitions[0].word
    for definition in definitions:
        if word.correct_definition_id == definition.id and reason != "duplicate":
            return "Cannot report the correct answer"
        if definition.hidden:
            return "At least one definition has already been removed"

    if len(definitions) == 1:
        try:
            report = models.Report.objects.get(
                reason=reason,
                definition_1_id=definitions[0].id
            )
        except models.Report.DoesNotExist:
            report = models.Report(
                reason=reason,
                word=word,
                definition_1_id=definitions[0].id,
                author_1=player
            )
            report.save()
            return JsonResponse({})
    else:
        try:
            report = models.Report.objects.get(
                reason=reason,
                definition_1_id=definitions[0].id,
                definition_2_id=definitions[1].id
            )
        except models.Report.DoesNotExist:
            report = models.Report(
                reason=reason,
                word=word,
                definition_1_id=definitions[0].id,
                definition_2_id=definitions[1].id,
                author_1=player
            )
            report.save()
            return JsonResponse({})

    report.submissions += 1
    if report.author_2 is None and report.author_1_id != player.id:
        report.author_2 = player
        report.submissions = 2
        report.save()
    elif report.author_3 is None and report.author_2_id != player.id and report.author_1_id != player.id:
        report.author_3 = player
        report.submissions = 3

        # Report confirmed by at least 3 different people
        # Commencing action
        if len(definitions) == 2:
            definitions[1].hidden = True
            definitions[1].save()
        else:
            definitions[0].hidden = True
            definitions[0].save()
        word.nrof_answers -= 1
        word.save()
        report.save()


@json_call
@requires_player
def report(request, player, data):
    # *How the Report System Works*
    # =============================
    # Basically, we will remove an answer when it has been reported by three
    # different people for the same reason
    # In order to store the number of reports we keep track of
    # author_1, author_2, author_3
    # If all authors are filled in we "hide" the answer with the highest id
    # This should ensure that the newest/non-correct answer is always removed
    reason = data["reason"]
    answers = data["answers"]
    answers.sort()

    if reason not in ("duplicate", "correct", "spam", "problematic"):
        return HttpResponseBadRequest("Invalid reporting reason")

    if len(answers) > 2 and reason != "correct":
        return HttpResponseBadRequest("Maximum of two answers are allowed")
    if len(answers) < 1:
        return HttpResponseBadRequest("Select at least one definition")

    if reason != "correct":
        message = str(handle_single_report(reason, answers, player))
    else:
        message = ""
        for answer in answers:
            message += str(handle_single_report(reason, [answer], player))

    return JsonResponse({"message": message})
