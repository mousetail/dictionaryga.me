let BundleTracker = require('webpack-bundle-tracker');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');

module.exports = {
  mode: "production",

  // Enable sourcemaps for debugging webpack's output.
  devtool: "source-map",

  resolve: {
    modules: ['node_modules', 'bower_components'],
    // Add '.ts' and '.tsx' as resolvable extensions.
    extensions: [".ts", ".tsx", '.js', '.jsx'],

    alias: {
      "@fortawesome/free-solid-svg-icons$": "@fortawesome/free-solid-svg-icons/index.es.js",
      "@fortawesome/free-regular-svg-icons$": "@fortawesome/free-regular-svg-icons/index.es.js"
    }
  },

  entry: {
    'index': './frontend/js/index',
    'style': './frontend/style.css'
  },

  output: {
    filename: "[name]-[hash].js",
    path: __dirname + "/static/bundles"
  },

  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "ts-loader"
          }
        ],
      },
      {
        test: /\.css/,
        exclude: /node_modules/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          'css-loader'
        ]
      },
      // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
      {
        enforce: "pre",
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "source-map-loader"
      }
    ]
  },

  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name]-[hash].css'
    }),
    new BundleTracker({
      filename: './webpack-stats.json'
    })
  ],

  optimization: {
    minimizer: [
      new TerserJSPlugin({}),
      new OptimizeCSSAssetsPlugin({})
    ]
  }
};